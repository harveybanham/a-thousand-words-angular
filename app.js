var fs = require('fs');
var https = require('https');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
var cors = require('cors');
var fs = require('fs');
var config = {};

if (fs.existsSync("./config.json")) {
    config = require("./config.json");
} else {
    config.BEARER_TOKEN = process.env.BEARER_TOKEN;
}

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static('build'));

//For Heroku
var server_port = process.env.PORT || 3000;

app.listen(server_port, function() {
    console.log('listening on ' + server_port);
});


app.get('/', function (req, res) {
    res.sendFile(__dirname + '/build/index.html');
});
app.get('/about', function (req, res) {
    res.sendFile(__dirname + '/build/index.html');
});
app.get('/contact', function (req, res) {
    res.sendFile(__dirname + '/build/index.html');
});

app.get('/get-tweets', function (req, res) {
    getTweets(req.query.tag, function(output) {
        res.send(output);
    });
});

app.get('/get-trends', function (req, res) {
    getTrends(function(output) {
        res.send(output);
    });
});




function getTweets(tag, callback) {
    var output = [];
    
    var request = {
        host: 'api.twitter.com',
        path: '/1.1/search/tweets.json?q='+encodeURIComponent(tag)+'%20filter%3Aimages%20filter%3Asafe&count=50',
        headers: {
            'User-Agent': 'a thousand words',
            Authorization: 'bearer ' + config.BEARER_TOKEN
        }
    };
    
    https.get(request, function(response) {
        var json = '';
        response.setEncoding('utf8');
        
        if(response.statusCode === 200) {
            response.on('data', function(chunk) {
                json += chunk;
            }).on('end', function() {
                var tweetObj = JSON.parse(json);
                // fs.writeFileSync("tweets.json", JSON.stringify(json));
                
                tweetObj.statuses.forEach(function(tweet) {
                    if(typeof(tweet.entities.media) !== "undefined"
                        && !tweet.possibly_sensitive) {
                        var outputObj = {};
                        outputObj.id = tweet.id_str;
                        outputObj.name = tweet.user.screen_name;
                        outputObj.text = tweet.text;
                        outputObj.imageurl = tweet.entities.media[0].media_url_https;
                        outputObj.url = tweet.entities.media[0].expanded_url;
                        output.push(outputObj);
                    }
                });
                
                callback(output);
            });
        } else {
            console.log('Error : ' + reseponse.statusCode);
        }
    }).on('error', function(e) {
        console.log('Error : ' + e.message);
    });
}

function getTrends(callback) {
    var output = [];
    
    var request = {
        host: 'api.twitter.com',
        path: '/1.1/trends/place.json?id=1',
        headers: {
            'User-Agent': 'a thousand words',
            Authorization: 'bearer ' + config.BEARER_TOKEN
        }
    };
    
    https.get(request, function(response) {
        var json = '';
        response.setEncoding('utf8');
        
        if(response.statusCode === 200) {
            response.on('data', function(chunk) {
                json += chunk;
            }).on('end', function() {
                var trendObj = JSON.parse(json);
                // fs.writeFileSync("trends.json", JSON.stringify(json));
                trendObj[0].trends.forEach(function(trend) {
                    var outputObj = {};
                    outputObj.name = trend.name;
                    outputObj.url = trend.url;
                    output.push(outputObj);
                });
                callback(output);
            });
        } else {
            console.log('Error : ' + reseponse.statusCode);
        }
    }).on('error', function(e) {
        console.log('Error : ' + e.message);
    });
}
