var app = angular.module('app', ['ngRoute', 'core']);

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
    .when('/', 
    {
        controller: 'homeController',
        templateUrl: 'components/home/homeView.html'
    })
    .when('/about', 
    {
        controller: 'aboutController',
        templateUrl: 'components/about/aboutView.html'
    })
    .when('/contact', 
    {
        controller: 'contactController',
        templateUrl: 'components/contact/contactView.html'
    })
    .otherwise( { redirectTo: '/' } );
    
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('');
});