app.controller("homeController", ['$scope', 'homeFactory', function($scope, homeFactory) {
    init();
    
    function init() {
        homeFactory.getTagTrends().then(function successCallback(response) {
            //Success, update scope trends data
            $scope.trends = response.data;
        }, function errorCallback(response) {
            console.log("Error: " + response);
        });
        
        //Set up masonry object
        var twitterList = $(".tweet-list");
        twitterList.masonry({
            columnWidth: ".grid-sizer",
            gutter: 50,
            itemSelector: ".tweet",
            percentPosition: true,
            transitionDuration: 0
        });
    }
    
    $scope.updateTweets = function(tag) {
        //Don't submit request if there is no search term
        if(tag == undefined) { return; }
        
        //Update search input
        $scope.searchInput = tag;
        homeFactory.getTweets(tag).then(function successCallback(response) {
            //Success, update scope trends data
            $scope.tweets = response.data;
            
            $scope.$watch('tweets', function(newVal, oldVal) {
                var twitterList = $(".tweet-list");
                twitterList.imagesLoaded(function() {
                    // console.log("IMAGES LOADED");
                    twitterList.masonry('reloadItems');
                    twitterList.masonry('layout');
                    $(".tweet").css("opacity", "1");
                });
            }, true);
        }, function errorCallback(response) {
            console.log("Error: " + response);
        });
    }
}]);