app.factory('homeFactory', function($http) {
    var factory = {};
    
    factory.getTagTrends = function() {
        var request = {
            method: 'GET',
            url: '/get-trends'
        };
        
        return $http(request);
    }
    
    factory.getTweets = function(tag) {
        var request = {
            method: 'GET',
            url: '/get-tweets?tag='+tag
        };
        
        return $http(request);
    }
    
    return factory;
});