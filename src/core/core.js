var core = angular.module('core', []);

core.filter('decode', function() {
    return function(input) {
      return decodeURIComponent(input);
    };
  });