app.controller("navigationController", ['$scope', '$location', function($scope, $location) {
    $scope.items = [
        {path: '/', title: 'Home'},
        {path: '/about', title: 'About'},
        {path: '/contact', title: 'Contact'}
    ];
    
    $scope.isActive = function(item) {
        if (item.path == $location.path()) {
            return true;
        }
        return false;
    };
}]);